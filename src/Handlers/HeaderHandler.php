<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpHttp\Handlers;

use GgcpHttp\Exceptions\HttpHeaderException;
use GgcpHttp\Support\TraceDataHelper;

class HeaderHandler
{
    public static function create()
    {
        return new self();
    }

    public function handle(array $options)
    {
        if (!function_exists('getallheaders')) {
            throw new HttpHeaderException('找不到系统内置的 getallheaders 函数.');
        }

        $headers = getallheaders();

        // 需要往所有发起请求的 Request Header 中添加的请求头参数
        $appendHeaders = [
            // 需要透传的 TraceId
            TraceDataHelper::TRACE_ID_FIELD  => TraceDataHelper::getTraceIdFromHeaders($headers),
            // 当前请求的 SpanId，需要告诉下游请求它的调用方 SpanId 是多少
            // 对于下游请求来说，是它的 ParentId
            TraceDataHelper::SPAN_ID_FIELD => TraceDataHelper::getCurrentSpanId(),
        ];

        $options['headers'] = !empty($options['headers']) ? array_merge($options['headers'], $appendHeaders) : $appendHeaders;
        return $options;
    }
}
