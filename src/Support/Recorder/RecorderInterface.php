<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpHttp\Support\Recorder;

interface RecorderInterface
{
    /**
     * 保存采集到的请求数据
     * 
     * @param  array $data 采集到的数据数组
     * @return mixed|false 保存结果，如果保存失败则返回 false，否则返回对应数据的保存唯一 ID
     */
    public function save(array $data);
}