<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpHttp\Support\Facades;

use GgcpHttp\Support\EmptyLogger;
use GgcpHttp\Support\Facade;
use GgcpHttp\Support\YiiLogger;

/**
 * @method static void info(string $message, string $category = 'application')
 * @method static void warning(string $message, string $category = 'application')
 * @method static void error(string $message, string $category = 'application')
 */
class Log extends Facade
{
    protected static function getFacadeAccessor()
    {
        if (class_exists('\\Yii')) {
            return YiiLogger::class;
        } else {
            return new EmptyLogger;
        }
    }
}
