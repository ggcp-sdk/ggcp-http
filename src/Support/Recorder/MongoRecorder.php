<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpHttp\Support\Recorder;

use GgcpHttp\Support\Facades\Log;
use GgcpHttp\Support\Traits\SingletonTrait;
use MongoDB\Client;

/**
 * @method static $this instance(array $config)
 */
class MongoRecorder implements RecorderInterface
{
    use SingletonTrait;

    /**
     * @var string
     */
    private $dbname = 'httpTrace';

    /**
     * @var string
     */
    private $collection = 'dataSet';

    /**
     * @var \MongoDB\Client
     */
    private $client;

    public function __construct(array $config)
    {
        $uri = $config['host'] ?? 'mongodb://127.0.0.1:27017';
        if (!empty($config['username'])) {
            $options = ['username' => $config['username'], 'password' => $config['password']];
        }
        if (!empty($config['dbname'])) {
            $this->dbname = $config['dbname'];
        }
        if (!empty($config['collection'])) {
            $this->collection = $config['collection'];
        }

        $options['connectTimeoutMS'] = $config['connectTimeout'];
        // Socket 数据传输默认超时时间，设置为连接超时时间的一半
        $options['socketTimeoutMS'] = (int) ceil($config['connectTimeout'] / 2);

        $this->client = new Client($uri, $options ?? []);
    }

    /**
     * @param  array $data
     * @return mixed|false
     */
    public function save(array $data)
    {
        /** @var \MongoDB\Collection */
        $collection = $this->client->{$this->dbname}->{$this->collection};
        try {
            /** @var \MongoDB\InsertOneResult */
            $res = $collection->insertOne($data);
            return !$res->getInsertedCount() ? false : $res->getInsertedId();

        } catch (\MongoDb\Driver\Exception\Exception $e) {
            Log::error('MongoDB 异常: ' . $e->getMessage());
            return false;
        }
    }
}
