<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpHttp\Tests;

use GgcpHttp\Client;
use GgcpHttp\Support\TraceDataHelper;
use PHPUnit\Framework\TestCase;

class StatCollectionTest extends TestCase
{
    /**
     * 测试验证请求处理器是否能正常准备调用前所需的信息
     */
    public function testPrepareOptions()
    {
        $options = [
            'collection' => [
                'engine'           => 'mongo',
                'options'          => [
                    'username' => 'root',
                    'password' => '',
                ],
                'onStatCollection' => [$this, 'vlaidateCollectionStat'],
            ],
        ];

        $_SERVER = array_merge($_SERVER, [
            'HTTP_X_TRACE_ID'    => TraceDataHelper::getTraceIdFromHeaders([]),
            'HTTP_X_SPAN_ID'     => TraceDataHelper::getSpanIdFromHeaders([]),
            'HTTP_X_SUB_SPAN_ID' => TraceDataHelper::getSubSpanIdFromHeaders([]),
        ]);

        $this->assertNotEmpty(getallheaders(), '单元测试 Headers 数据不足');

        return $options;
    }

    /**
     * @depends testPrepareOptions
     */
    public function testRequestStatCollection(array $options)
    {
        $options['collection']['onStatCollection'] = [$this, 'validateSuccessfullyRequestStat'];
        $successRequestClient                      = Client::prepare($options);

        $successRequestClient->getSender()->get('https://mirrors.aliyun.com/composer/p2/mingyuanyun/ggcp-http.json');

        $options['collection']['onStatCollection'] = [$this, 'validateFailureRequestStat'];
        $successRequestClient                      = Client::prepare($options);

        $successRequestClient->getSender()->get('https://mirrors.aliyun.com/composer/p2/mingyuanyun/ggcp-http1.json');
    }

    /**
     * 验证一个正常的接口请求是否能正常采集到数据
     */
    public function validateSuccessfullyRequestStat(array $stats)
    {
        $this->validateBaseStat($stats);

        $this->assertNotEmpty($stats['responseStat'], '未能成功采集 Response 信息');
    }

    /**
     * 验证一个异常的接口请求是否能正常采集到数据
     */
    public function validateFailureRequestStat(array $stats)
    {
        $this->validateBaseStat($stats);

        $this->assertNotEmpty('errMsg', $stats['errMsg'], '未能成功采集接口异常信息');
    }

    /**
     * 校验采集到的数据中，各项重要指标是否内容都正常
     */
    public function validateBaseStat(array $stats)
    {
        $this->assertNotEmpty($stats['traceId'], '调用链穿透 TraceId 异常');
        $this->assertNotEmpty($stats['spanId'], '当前子请求 SpanId 异常');

        $this->assertNotEmpty($stats['duration'], '未能成功记录请求开始/结束时间');
        if (!empty($stats['duration'])) {
            $this->assertGreaterThan($stats['duration']['startTime'], $stats['duration']['endTime'], '请求记录结束时间不大于开始时间');
        }

        $this->assertNotEmpty($stats['requestStat'], '未能成功采集 Request 信息');
        if (!empty($stats['requestStat'])) {
            $this->assertNotEmpty($stats['requestStat']['scheme'], '未能成功采集请求 Scheme 信息');
            $this->assertNotEmpty($stats['requestStat']['host'], '未能成功采集请求 Host 信息');
            $this->assertNotEmpty($stats['requestStat']['url'], '未能成功采集请求 URL 信息');
            $this->assertNotEmpty($stats['requestStat']['method'], '未能成功采集请求 Method 信息');
            $this->assertNotEmpty($stats['requestStat']['header'], '未能成功采集请求 Header 信息');
        }
    }
}
