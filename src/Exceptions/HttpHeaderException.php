<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpHttp\Exceptions;

use GuzzleHttp\Exception\TransferException;

class HttpHeaderException extends TransferException
{
    
}