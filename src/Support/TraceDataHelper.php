<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpHttp\Support;

class TraceDataHelper
{
    // 调用链透传唯一 ID，整条链路下一致，放在请求的 Request Header 中
    const TRACE_ID_FIELD = 'X-Trace-Id';
    // 调用方的 SpanId，对于被调用方（子请求）来说，是它的 ParentId
    const SPAN_ID_FIELD = 'X-Span-Id';
    // 调用方分配给子请求的 SpanId，被调用方可以根据情况选择是否使用
    const SUB_SPAN_ID_FIELD = 'X-Sub-Span-Id';

    /**
     * @return bool
     */
    protected static function isYiiFramework()
    {
        return class_exists('\\Yii');
    }

    /**
     * @param  array $headers
     * @return string
     */
    public static function getTraceIdFromHeaders(array $headers)
    {
        $hv = $headers[self::TRACE_ID_FIELD] ?? '';
        if (empty($hv) && self::isYiiFramework()) {
            // 尝试从 Request Headers 中获取 ilog 设置的穿透 ID 值
            // 如果从 Headers 取不到，则利用当前请求的 ilogId 作为穿透 ID
            $hv = $headers[self::getTraceIdFieldFromILog()] ?? self::getTraceIdFromILog();
        }

        $traceId = is_array($hv) ? $hv[0] : $hv;
        return $traceId ?: uniqid('ggcphttp_');
    }

    /**
     * @param  array $headers
     * @return string
     */
    public static function getSpanIdFromHeaders(array $headers)
    {
        $hv = $headers[self::SPAN_ID_FIELD] ?? '';
        if (empty($hv) && self::isYiiFramework()) {
            // 尝试从 Request Headers 中获取 ilog 设置的 SpanID 值
            // 如果从 Headers 取不到，则利用当前请求的 subLogId 作为 SpanID
            $hv = $headers[self::getSpanIdFieldFromILog()] ?? self::getSpanIdFromILog();
        }

        $spanId = is_array($hv) ? $hv[0] : $hv;
        return $spanId ?: uniqid('ggcphttp_');
    }

    /**
     * @param  array $headers
     * @return string
     */
    public static function getSubSpanIdFromHeaders(array $headers)
    {
        $hv        = $headers[self::SUB_SPAN_ID_FIELD] ?? '';
        $subSpanId = is_array($hv) ? $hv[0] : $hv;
        return $subSpanId ?: uniqid('ggcphttp_');
    }

    /**
     * 获取当前请求的 SpanId
     *
     * @return string
     */
    public static function getCurrentSpanId()
    {
        static $id;
        if (!empty($id)) {
            return $id;
        }

        if (self::isYiiFramework()) {
            $id = self::getSpanIdFromILog();
        } else {
            $id = uniqid('ggcphttp_');
        }

        return $id;
    }

    /**
     * 获取当前请求的 URL 链接
     *
     * @return string
     */
    public static function getCurrentUrl()
    {
        $url = $_SERVER['REQUEST_URI'] ?? '';
        if (array_key_exists('HTTP_X_FORWARDED_HOST', $_SERVER) || array_key_exists('HTTP_HOST', $_SERVER)) {
            // 拼接完整的 url 数据
            $scheme = $_SERVER['HTTP_X_FORWARDED_PROTO'] ?? ($_SERVER['REQUEST_SCHEME'] ?? 'https');
            $host   = $_SERVER['HTTP_X_FORWARDED_HOST'] ?? ($_SERVER['HTTP_HOST'] ?? '');
            $url    = sprintf("%s://%s%s", $scheme, $host, $url);
        }
        return $url;
    }

    // 内部接口响应头中，可能使用的 ilog sublogid 字段名
    protected static $responseLogIdFields = ['x-sublogid', 'X-Span-Id'];

    /**
     * 从请求响应的 Response Headers 数组中，获取子请求自己生成的 SpanId 数据
     *
     * @param  array $headers
     * @return string
     */
    public static function getResponseSpanId(array $responseHeaders)
    {
        $hv = '';
        // 优先根据默认的几个字段值尝试从 Header 数组中获取数据
        // 避免当前服务不是使用了 iLog 的 Yii2 框架时，无法自动获取框架设置的数据
        foreach (self::$responseLogIdFields as $f) {
            $hv = $responseHeaders[$f] ?? $hv;
        }

        // 尝试根据当前框架中 iLog 的配置情况，获取子请求响应的 SpanId
        if (empty($hv) && self::isYiiFramework()) {
            $hv = $responseHeaders[self::getSpanIdFieldFromILog()] ?? '';
        }

        $resSpanId = is_array($hv) ? $hv[0] : $hv;
        return $resSpanId ?? '';
    }

    /**
     * 根据 iLog 情况获取调用链追踪 TraceID 的 Header 字段名
     *
     * @return string
     */
    protected static function getTraceIdFieldFromILog()
    {
        if (\Yii::$app->request instanceof \yii\web\Request) {
            /** @var \iLog\Module */
            $ilog = \Yii::$app->getModule('iLog');
            return (!is_null($ilog) && property_exists($ilog, 'logIdFlag')) ? $ilog->logIdFlag : self::TRACE_ID_FIELD;
        } elseif (\Yii::$app->request instanceof \yii\console\Request) {
            /** @var \iLog\Component */
            $ilog = \Yii::$app->getComponents(false)['iLog'];
            return (!is_null($ilog) && property_exists($ilog, 'logIdFlag')) ? $ilog->logIdFlag : self::TRACE_ID_FIELD;
        } else {
            return self::TRACE_ID_FIELD;
        }
    }

    /**
     * 根据 iLog 情况获取对应的 ilog_id 数据
     *
     * @return string
     */
    protected static function getTraceIdFromILog()
    {
        if (\Yii::$app->request instanceof \yii\web\Request) {
            /** @var \iLog\Module */
            $ilog = \Yii::$app->getModule('iLog');
            return (!is_null($ilog) && property_exists($ilog, 'logId')) ? $ilog->logId : '';
        } elseif (\Yii::$app->request instanceof \yii\console\Request) {
            /** @var \iLog\Component */
            $ilog = \Yii::$app->getComponents(false)['iLog'];
            return (!is_null($ilog) && property_exists($ilog, 'logId')) ? $ilog->logId : '';
        } else {
            return '';
        }
    }

    /**
     * 根据 iLog 情况获取调用链追踪 SpanID 的 Header 字段名
     *
     * @return string
     */
    protected static function getSpanIdFieldFromILog()
    {
        if (\Yii::$app->request instanceof \yii\web\Request) {
            /** @var \iLog\Module */
            $ilog = \Yii::$app->getModule('iLog');
            return (!is_null($ilog) && property_exists($ilog, 'subLogIdFlag')) ? $ilog->subLogIdFlag : self::SPAN_ID_FIELD;
        } elseif (\Yii::$app->request instanceof \yii\console\Request) {
            /** @var \iLog\Component */
            $ilog = \Yii::$app->getComponents(false)['iLog'];
            return (!is_null($ilog) && property_exists($ilog, 'subLogIdFlag')) ? $ilog->subLogIdFlag : self::SPAN_ID_FIELD;
        } else {
            return self::SPAN_ID_FIELD;
        }
    }
    /**
     * 根据 iLog 情况获取当前请求的 SpanId，一般对应 iLog 中的 subLogId
     *
     * @return string
     */
    protected static function getSpanIdFromILog()
    {
        if (\Yii::$app->request instanceof \yii\web\Request) {
            /** @var \iLog\Module */
            $ilog = \Yii::$app->getModule('iLog');
            return (!is_null($ilog) && property_exists($ilog, 'subLogId')) ? $ilog->subLogId : '';
        } elseif (\Yii::$app->request instanceof \yii\console\Request) {
            /** @var \iLog\Component */
            $ilog = \Yii::$app->getComponents(false)['iLog'];
            return (!is_null($ilog) && property_exists($ilog, 'subLogId')) ? $ilog->subLogId : '';
        } else {
            return '';
        }
    }
}
