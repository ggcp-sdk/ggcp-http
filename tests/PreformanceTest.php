<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpHttp\Tests;

use function GuzzleHttp\Promise\unwrap;
use GgcpHttp\Client;
use GgcpHttp\Response;
use GgcpHttp\Support\TraceDataHelper;
use PHPUnit\Framework\TestCase;

class PreformanceTest extends TestCase
{
    /** 对提供接口的重复请求次数 */
    protected $invokeCount = 100;

    protected $abTestGetApi = 'https://mirrors.aliyun.com/composer/p2/mingyuanyun/ggcp-http.json';

    protected $abTestPostApi = 'https://mirrors.aliyun.com/composer/p2/mingyuanyun/ggcp-http.json';

    /**
     * 测试验证请求处理器是否能正常准备调用前所需的信息
     */
    public function testPrepareOptions()
    {
        $options = [
            'collection' => [
                'engine'      => 'mongo',
                'options'     => [
                    'username' => 'root',
                    'password' => '',
                ],
                'isOutputLog' => false,
            ],
        ];

        $_SERVER = array_merge($_SERVER, [
            'HTTP_X_TRACE_ID'    => TraceDataHelper::getTraceIdFromHeaders([]),
            'HTTP_X_SPAN_ID'     => TraceDataHelper::getSpanIdFromHeaders([]),
            'HTTP_X_SUB_SPAN_ID' => TraceDataHelper::getSubSpanIdFromHeaders([]),
        ]);

        $this->assertNotEmpty($options, '配置信息异常');

        return $options;
    }

    /**
     * @depends testPrepareOptions
     */
    public function testGetRequest(array $options)
    {
        $client = Client::prepare($options);

        // 通过原生 CURL 重复调用指定接口一定次数，记录执行耗时
        $curlStartTime = microtime(true);
        for ($i = 0; $i < $this->invokeCount; $i++) {
            $this->get($this->abTestGetApi);
        }
        $curlDuration = (microtime(true) - $curlStartTime) * 1000;

        // 通过 GgcpHttp 扩展重复调用指定接口一定次数，记录执行耗时
        $gtStartTime = microtime(true);
        for ($i = 0; $i < $this->invokeCount; $i++) {
            $client->getSender()->get($this->abTestGetApi);
        }
        $gtDuration = (microtime(true) - $gtStartTime) * 1000;

        $this->outputTable('同步 Get 请求调用', $gtDuration, $curlDuration);

        $this->assertNull(null, 'ok');
    }

    /**
     * @depends testPrepareOptions
     */
    public function testPostRequest(array $options)
    {
        $client = Client::prepare($options);

        // 通过原生 CURL 重复调用指定接口一定次数，记录执行耗时
        $curlStartTime = microtime(true);
        for ($i = 0; $i < $this->invokeCount; $i++) {
            $this->post($this->abTestGetApi, ['msg' => 'Hello']);
        }
        $curlDuration = (microtime(true) - $curlStartTime) * 1000;

        // 通过 GgcpHttp 扩展重复调用指定接口一定次数，记录执行耗时
        $gtStartTime = microtime(true);
        for ($i = 0; $i < $this->invokeCount; $i++) {
            $client->getSender()->post($this->abTestGetApi);
        }
        $gtDuration = (microtime(true) - $gtStartTime) * 1000;

        $this->outputTable('同步 Post 请求调用', $gtDuration, $curlDuration);

        $this->assertNull(null, 'ok');
    }

    /**
     * @depends testPrepareOptions
     */
    public function testGetAsyncRequest(array $options)
    {
        $client = Client::prepare($options);

        // 通过原生 CURL 重复调用指定接口一定次数，记录执行耗时
        $curlStartTime = microtime(true);
        for ($i = 0; $i < $this->invokeCount; $i++) {
            $this->get($this->abTestGetApi, ['msg' => 'Hello']);
        }
        $curlDuration = (microtime(true) - $curlStartTime) * 1000;

        $options = [
            // 异步调用所必须的回调配置
            'success' => function (Response $response) {},
        ];

        // 通过 GgcpHttp 扩展重复调用指定接口一定次数，记录执行耗时
        $gtStartTime = microtime(true);
        for ($i = 0; $i < $this->invokeCount; $i++) {
            $asyncApis["api{$i}"] = $client->getSender()->getAsync($this->abTestGetApi, [], $options);
        }
        unwrap($asyncApis);
        $gtDuration = (microtime(true) - $gtStartTime) * 1000;

        $this->outputTable('异步 Get 请求调用', $gtDuration, $curlDuration);

        $this->assertNull(null, 'ok');
    }

    /**
     * @depends testPrepareOptions
     */
    public function testPostAsyncRequest(array $options)
    {
        $client = Client::prepare($options);

        // 通过原生 CURL 重复调用指定接口一定次数，记录执行耗时
        $curlStartTime = microtime(true);
        for ($i = 0; $i < $this->invokeCount; $i++) {
            $this->post($this->abTestGetApi, ['msg' => 'Hello']);
        }
        $curlDuration = (microtime(true) - $curlStartTime) * 1000;

        $options = [
            // 异步调用所必须的回调配置
            'success' => function (Response $response) {},
        ];

        // 通过 GgcpHttp 扩展重复调用指定接口一定次数，记录执行耗时
        $gtStartTime = microtime(true);
        for ($i = 0; $i < $this->invokeCount; $i++) {
            $asyncApis["api{$i}"] = $client->getSender()->postAsync($this->abTestGetApi, null, $options);
        }
        unwrap($asyncApis);
        $gtDuration = (microtime(true) - $gtStartTime) * 1000;

        $this->outputTable('异步 Post 请求调用', $gtDuration, $curlDuration);

        $this->assertNull(null, 'ok');
    }

    public function get($url, $ch = null)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, 500);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 3000);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $res = curl_exec($ch);
        curl_close($ch);
        return $res;
    }

    public function post($url, $post)
    {
        $post = json_encode($post);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, 500);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 3000);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json; charset=utf-8']);

        $res = curl_exec($ch);
        curl_close($ch);
        return $res;
    }

    /**
     * @param  float $gtDuration
     * @param  float $curlDuration
     * @return void
     */
    protected function outputTable($title, $gtDuration, $curlDuration)
    {
        $onceGtDuration   = bcdiv($gtDuration, $this->invokeCount, 3);
        $onceCurlDuration = bcdiv($curlDuration, $this->invokeCount, 3);

        echo "{$title}\n";
        echo "------------------------------------------------------------------------------------------------------------------------------\n";
        echo "| GgcpHttp 耗时\t| CURL 耗时\t| GgcpHttp 平均耗时\t| CURL 平均耗时\t\t| {$this->invokeCount} 次调用性能差异\t| 单次调用性能差异\t|\n";
        echo sprintf(
            "| %.3fms\t| %.3fms\t| %.3fms\t\t| %.3fms\t\t| %.3fms\t\t| %.3fms\t\t|\n",
            $gtDuration,
            $curlDuration,
            $onceGtDuration,
            $onceCurlDuration,
            $gtDuration - $curlDuration,
            $onceGtDuration - $onceCurlDuration
        );
        echo "------------------------------------------------------------------------------------------------------------------------------\n\n";
    }
}
