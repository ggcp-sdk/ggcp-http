<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpHttp\Support\Recorder;

class RecorderFactory
{
    public static function make($name, array $config = [])
    {
        if ($name === 'mongo') {
            return new MongoRecorder($config);
        }
    }
}