<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpHttp;

use GgcpHttp\Support\Facades\Log;
use GgcpHttp\Support\TraceDataHelper;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise\Promise;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

class Sender
{
    /**
     * @var \GuzzleHttp\Client
     */
    private $handler;

    public function __construct(\GuzzleHttp\Client $handler)
    {
        $this->handler = $handler;
    }

    /**
     * 发起一个 GET 请求调用。请求选项 $options 的使用，请参考
     * https://guzzle-cn.readthedocs.io/zh_CN/latest/request-options.html
     *
     * @param  string $uri      请求地址的 uri
     * @param  array  $query    GET 请求参数数组
     * @param  array  $options  GuzzleHttp 所需的请求配置
     * @return Response
     */
    public function get($uri, array $query = [], array $options = [])
    {
        if (!empty($query)) {
            $options[RequestOptions::QUERY] = $query;
        }
        return $this->request('get', $uri, $options);
    }

    /**
     * 发起一个 POST 请求调用。请求选项 $options 的使用，请参考
     * https://guzzle-cn.readthedocs.io/zh_CN/latest/request-options.html
     *
     * @param  string       $uri      请求地址的 uri
     * @param  array|string $data     Body 数据
     * @param  array        $options  GuzzleHttp 所需的请求配置
     * @return Response
     */
    public function post($uri, $data = null, array $options = [])
    {
        if (!is_null($data)) {
            $options['body'] = $data;
        }
        return $this->request('post', $uri, $options);
    }

    /**
     * 发起一个 POST 请求调用，Body 数据体的内容传递将采用 application/json
     * 的格式。请求选项 $options 的使用，请参考
     * https://guzzle-cn.readthedocs.io/zh_CN/latest/request-options.html
     *
     * @param  string $uri      请求地址的 uri
     * @param  array  $data     Body 数据，数组会被转成 json 格式发出去
     * @param  array  $options  GuzzleHttp 所需的请求配置
     * @return Response
     */
    public function postWithJson($uri, array $data = [], array $options = [])
    {
        $options[RequestOptions::JSON] = $data;
        return $this->request('post', $uri, $options);
    }

    /**
     * 发起一个 POST 请求调用，Body 数据体的内容传递将采用 application/x-www-from-urlencoded
     * 的格式。请求选项 $options 的使用，请参考
     * https://guzzle-cn.readthedocs.io/zh_CN/latest/request-options.html
     *
     * @param  string $uri      请求地址的 uri
     * @param  array  $data     Body 数据，数组会被转成 form 格式发出去
     * @param  array  $options  GuzzleHttp 所需的请求配置
     * @return Response
     */
    public function postWithForm($uri, array $data = [], array $options = [])
    {
        $options[RequestOptions::FORM_PARAMS] = $data;
        return $this->request('post', $uri, $options);
    }

    /**
     * 发起一个请求调用，可根据需要自行传入对应的请求 Method 类型以及相应的请求数据。
     * 请求选项 $options 的使用，请参考 https://guzzle-cn.readthedocs.io/zh_CN/latest/request-options.html
     *
     * @param  string $method   HTTP method
     * @param  string $uri      请求地址的 uri
     * @param  array  $options  GuzzleHttp 所需的请求配置
     * @return Response
     * @throws \Exception
     */
    public function request($method, $uri, array $options = [])
    {
        // 给每个子请求设置一个唯一的 SpanId，将此 id 放到 request header 中，方便逻辑透传
        $subSpanId      = uniqid('ggcphttp_');
        $subSpanIdField = TraceDataHelper::SUB_SPAN_ID_FIELD;
        if (!empty($options['headers'][$subSpanIdField])) {
            // 如果业务传递了指定的子请求 SpanId，则以业务传递的为准
            $subSpanId = $options['headers'][$subSpanIdField];
        } else {
            $options['headers'][$subSpanIdField] = $subSpanId;
        }

        try {
            // 记录请求开始发起的时间
            StatCollection::instance()->markRequestStartTime($subSpanId);

            $response = $this->handler->request($method, $uri, $options);
            return new Response($response);

        } catch (RequestException $e) {
            // 如果是接口请求异常，则将相关异常信息记录到采集器中
            StatCollection::instance()->pushErrorInfo($subSpanId, $e->getMessage());
            // 返回一个可以用于业务判断的 Response 空实例，减少接口调用后的接口失败业务判断代码
            return new Response($e->getResponse());

        } catch (\Exception $e) {
            // 如果是系统异常，则将异常信息记录到本地日志
            Log::error($e->getMessage() . ' ' . $e->getTraceAsString());
            // 将异常原封不动向上抛出，该让业务发现的异常还是让业务清楚的知道，避免存在暗坑
            throw $e;

        } finally {
            // 请求结束，触发采集数据的保存
            StatCollection::instance()->markRequestEndTime($subSpanId);
            StatCollection::instance()->sendAndClean($subSpanId);
        }
    }

    /**
     * 通过异步的方式发起一个 GET 请求调用。请求选项 $options 的使用，请参考
     * https://guzzle-cn.readthedocs.io/zh_CN/latest/request-options.html
     *
     * @param  string $uri      请求地址的 uri
     * @param  array  $query    GET 请求参数数组
     * @param  array  $options  GuzzleHttp 所需的请求配置
     * @return Promise|PromiseInterface
     */
    public function getAsync($uri, array $query = [], array $options = [])
    {
        if (!empty($query)) {
            $options[RequestOptions::QUERY] = $query;
        }
        return $this->requestAsync('get', $uri, $options);
    }

    /**
     * 通过异步的方式发起一个 POST 请求调用。请求选项 $options 的使用，请参考
     * https://guzzle-cn.readthedocs.io/zh_CN/latest/request-options.html
     *
     * @param  string $uri      请求地址的 uri
     * @param  array  $options  GuzzleHttp 所需的请求配置
     * @return Promise|PromiseInterface
     */
    public function postAsync($uri, $data = null, array $options = [])
    {
        if (!is_null($data)) {
            $options['body'] = $data;
        }
        return $this->requestAsync('post', $uri, $options);
    }

    /**
     * 通过异步的方式发起一个 POST 请求调用，Body 数据体的内容传递将采用 application/json
     * 的格式。请求选项 $options 的使用，请参考
     * https://guzzle-cn.readthedocs.io/zh_CN/latest/request-options.html
     *
     * @param  string $uri      请求地址的 uri
     * @param  array  $data     Body 数据，数组会被转成 json 格式发出去
     * @param  array  $options  GuzzleHttp 所需的请求配置
     * @return Promise|PromiseInterface
     */
    public function postAsyncWithJson($uri, array $data = [], array $options = [])
    {
        $options[RequestOptions::JSON] = $data;
        return $this->requestAsync('post', $uri, $options);
    }

    /**
     * 通过异步的方式发起一个 POST 请求调用，Body 数据体的内容传递将采用 application/x-www-from-urlencoded
     * 的格式。请求选项 $options 的使用，请参考
     * https://guzzle-cn.readthedocs.io/zh_CN/latest/request-options.html
     *
     * @param  string $uri      请求地址的 uri
     * @param  array  $data     Body 数据，数组会被转成 form 格式发出去
     * @param  array  $options  GuzzleHttp 所需的请求配置
     * @return Promise|PromiseInterface
     */
    public function postAsyncWithForm($uri, array $data = [], array $options = [])
    {
        $options[RequestOptions::FORM_PARAMS] = $data;
        return $this->requestAsync('post', $uri, $options);
    }

    /**
     * 通过异步的方式发起一个请求调用，可根据需要自行传入对应的请求 Method 类型以及相应的请求数据。
     * 请求选项 $options 的使用，请参考 https://guzzle-cn.readthedocs.io/zh_CN/latest/request-options.html
     *
     * @param  string $method   HTTP method
     * @param  string $uri      请求地址的 uri
     * @param  array  $options  GuzzleHttp 所需的请求配置
     * @return Promise|PromiseInterface
     */
    public function requestAsync($method, $uri, array $options = [])
    {
        // 给每个子请求设置一个唯一的 SpanId，将此 id 放到 request header 中，方便逻辑透传
        $subSpanId      = uniqid('ggcphttp_');
        $subSpanIdField = TraceDataHelper::SUB_SPAN_ID_FIELD;
        if (!empty($options['headers'][$subSpanIdField])) {
            // 如果业务传递了指定的子请求 SpanId，则以业务传递的为准
            $subSpanId = $options['headers'][$subSpanIdField];
        } else {
            $options['headers'][$subSpanIdField] = $subSpanId;
        }

        // 记录请求开始的时间
        StatCollection::instance()->markRequestStartTime($subSpanId);

        $promise = $this->handler->requestAsync($method, $uri, $options);
        return $promise->then(
            function (ResponseInterface $response) use ($options, $subSpanId) {
                // 请求结束，触发采集数据的保存
                StatCollection::instance()->markRequestEndTime($subSpanId);
                StatCollection::instance()->sendAndClean($subSpanId);

                $res = new Response($response);
                // 如果注册了接口成功时的回调函数，则返回回调函数的执行结果
                // 否则直接返回封装的 Response 实例
                if (isset($options['success']) && is_callable($options['success'])) {
                    return call_user_func($options['success'], $res);
                }
                return $res;
            },
            function (RequestException $e) use ($options, $subSpanId) {
                // 如果是接口请求异常，则将相关异常信息记录到采集器中
                StatCollection::instance()->pushErrorInfo($subSpanId, $e->getMessage());
                StatCollection::instance()->markRequestEndTime($subSpanId);
                StatCollection::instance()->sendAndClean($subSpanId);

                if (isset($options['failure']) && is_callable($options['failure'])) {
                    call_user_func($options['failure'], new Response($e->getResponse()), $e);
                }
            }
        );
    }
}
