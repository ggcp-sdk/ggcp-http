<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpHttp\Tests;

use GgcpHttp\Client;
use GgcpHttp\Response;
use GgcpHttp\Support\TraceDataHelper;
use GuzzleHttp\Promise\Utils;
use PHPUnit\Framework\TestCase;

class SendRequestTest extends TestCase
{
    /**
     * 测试验证请求处理器是否能正常准备调用前所需的信息
     */
    public function testPrepareOptions()
    {
        $options = [
            'collection' => [
                'engine'  => 'mongo',
                'options' => [
                    'username' => 'root',
                    'password' => '',
                ],
            ],
        ];

        $client  = Client::prepare($options);
        $options = $client->getOptions();

        $this->assertNotEmpty($options, '未能成功初始化 options 信息');
        $this->assertIsCallable($options['on_stats'], '未能成功注册监控数据分析回调处理器');
        $this->assertIsArray($options['headers'], '未能成功注入调用链所需 Header 数据');
        $this->assertNotEmpty($options['connect_timeout'], '连接超时默认值异常');
        $this->assertNotEmpty($options['timeout'], '等待响应超时默认值异常');

        $this->assertArrayHasKey(TraceDataHelper::TRACE_ID_FIELD, $options['headers'], '未能成功注入 Trace Id');
        $this->assertArrayHasKey(TraceDataHelper::SPAN_ID_FIELD, $options['headers'], '未能成功注入来源 Span Id');

        return $client;
    }

    /**
     * @depends testPrepareOptions
     */
    public function testSendRequestSuccess(Client $client)
    {
        $st       = microtime(true);
        $response = $client->getSender()->get('https://mirrors.aliyun.com/composer/p2/mingyuanyun/ggcp-http.json');
        $onceTime = microtime(true) - $st;

        $this->assertInstanceOf(\GgcpHttp\Response::class, $response, '未能正常返回接口调用结果');
        // $this->assertJson($response->isSuccess(), '接口返回异常');
        // $this->assertIsArray($response->toArray(), '接口返回数据解析异常');

        return $onceTime;
    }

    /**
     * 接口请求失败的场景测试
     * 
     * @depends testPrepareOptions
     */
    public function testSendRequestFailure(Client $client)
    {
        $response = $client->getSender()->get('https://mirrors.aliyun.com/composer/p2/mingyuanyun/ggcp-http1.json');
        $this->assertFalse($response->isSuccess(), '接口异常时返回数据类型不正确');
    }

    /**
     * @depends testPrepareOptions
     * @depends testSendRequestSuccess
     */
    public function testAsyncSendRequest(Client $client, $onceTime)
    {
        $st = microtime(true);

        $options = [
            // 'success' => function (Response $response) use ($st) {
            //     $this->assertTrue($response->isSuccess(), '异步接口请求失败');
            //     return true;
            // },
        ];

        $promises = [
            'api1' => $client->getSender()->getAsync('https://mirrors.aliyun.com/composer/p2/mingyuanyun/ggcp-http.json', [], $options),
            'api2' => $client->getSender()->getAsync('https://mirrors.aliyun.com/composer/p2/mingyuanyun/ggcp-http.json', [], $options),
            'api3' => $client->getSender()->getAsync('https://mirrors.aliyun.com/composer/p2/mingyuanyun/ggcp-http.json', [], $options),
            'api4' => $client->getSender()->getAsync('https://mirrors.aliyun.com/composer/p2/mingyuanyun/ggcp-http.json', [], $options),
            'api5' => $client->getSender()->getAsync('https://mirrors.aliyun.com/composer/p2/mingyuanyun/ggcp-http.json', [], $options),
            'api6' => $client->getSender()->getAsync('https://mirrors.aliyun.com/composer/p2/mingyuanyun/ggcp-http.json', [], $options),
        ];

        $results = Utils::unwrap($promises);

        foreach ($results as $res) {
            $this->assertInstanceOf(Response::class, $res, '异步接口未能成功拿到 success 回调的处理结果');
        }

        $time = microtime(true) - $st;
        $this->assertLessThan($onceTime * 6, $time, '并发请求耗时未成功减少');
    }

    public function testTimeoutRequest()
    {
        $st = time();
        
        $options = ['connect_timeout' => 1, 'timeout' => 2];
        Client::prepare($options)->getSender()->get('https://www.google.com');

        $et = time();
        $this->assertLessThan(3, $et - $st, '超时设置不起作用,实际耗时', $et - $st);
    }
}
