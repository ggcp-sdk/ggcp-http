<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpHttp\Exceptions;

use RuntimeException;

class AsyncRequestException extends RuntimeException
{
    
}