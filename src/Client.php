<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpHttp;

use GgcpHttp\Handlers\HeaderHandler;
use GgcpHttp\Handlers\StatHandler;
use GuzzleHttp\RequestOptions;

final class Client
{
    /**
     * @var \GuzzleHttp\Client
     */
    private $handler;

    /**
     * @var Sender
     */
    private $sender;

    /**
     * @var array
     */
    private $options = [];

    /**
     * 接口请求默认配置项内容，调用方没有设置对应值时，默认使用的一些配置项值
     * 
     * @var array
     */
    private $defaultOptions = [
        // 默认请求建立连接 5s 超时
        RequestOptions::CONNECT_TIMEOUT => 5,
        // 默认请求等待响应 30s 超时
        RequestOptions::TIMEOUT => 30,
        // 默认开启 SSL 证书验证
        RequestOptions::VERIFY => true,
        
    ];

    public function __construct($options = [])
    {
        if (!empty($options['baseUri'])) {
            $options['base_uri'] = $options['baseUri'];
            unset($options['baseUri']);
        }
        if (isset($options['collection'])) {
            $this->initCollection($options['collection']);
            unset($options['collection']);
        }

        // 加入默认配置项的支持，避免调用方不设置的情况下产生大量的接口等待阻塞业务
        $this->options = array_merge($this->defaultOptions, $options);
    }

    /**
     * 准备一个可以用于发起请求调用的请求调用器实例
     *
     * @param  array $options   创建请求器所需的一些基础配置信息
     * @return $this
     */
    public static function prepare(array $options = [])
    {
        $client = new self($options);

        // 请求处理器准备动作主要做两件事情：1. 注册 curl 信息采集回调 2. 设置 Header 头信息
        $client->bootstrapStatsHandler(StatHandler::create());
        $client->bootstrapHeaderHandler(HeaderHandler::create());

        // 根据 options 信息创建对应的 GuzzleHttp\Client 实例，后续可以根据
        // 同一个 Client 实例进行请求的调用
        $client->prepareGuzzleHttpClient();

        return $client;
    }

    /**
     * @return void
     */
    protected function prepareGuzzleHttpClient()
    {
        if (is_null($this->handler)) {
            $this->handler = new \GuzzleHttp\Client($this->options);
        }
    }

    /**
     * 获取一个请求发起处理器，通过处理器可以发起常见的 GET、POST、PUT、DELETE 等 Http 请求
     *
     * @return Sender
     */
    public function getSender()
    {
        $this->prepareGuzzleHttpClient();

        if (is_null($this->sender)) {
            $this->sender = new Sender($this->handler);
        }
        return $this->sender;
    }

    /**
     * 在当前请求节点中，注册向下游发起请求的统一 Header 信息。相关信息会被整理到
     * GuzzleHttp 所需的 options.headers 配置数据中
     *
     * @param  HeaderHandler $handler
     * @return void
     */
    public function bootstrapHeaderHandler(HeaderHandler $handler)
    {
        $this->options = $handler->handle($this->options);
    }

    /**
     * 往 GuzzleHttp 的请求选项 options 中，注册一个 on_stats 的回调处理，用于
     * 采集发起的每个请求网络传输耗时等指标信息
     *
     * @param  StatHandler $handler
     * @return void
     */
    public function bootstrapStatsHandler(StatHandler $handler)
    {
        unset($this->options['on_stats']);
        $this->options['on_stats'] = [$handler, 'handle'];
    }

    /**
     * 获取当前设置的 options 信息
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * 判断是否已经设置过采集器信息
     *
     * @return bool
     */
    public static function hasSetCollection()
    {
        return StatCollection::instance()->isPrepareRecorder();
    }

    /**
     * 设置数据采集器信息
     *
     * @param  array $options   采集器配置信息
     * @return void
     */
    public static function initCollection(array $options)
    {
        if (!self::hasSetCollection()) {
            StatCollection::instance()->init($options);
        }
    }
}
