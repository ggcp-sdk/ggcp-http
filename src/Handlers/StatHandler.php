<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpHttp\Handlers;

use GgcpHttp\StatCollection;
use GgcpHttp\Support\TraceDataHelper;
use GuzzleHttp\TransferStats;

class StatHandler
{
    public static function create()
    {
        return new self();
    }

    public function handle(TransferStats $stats)
    {
        $request = $stats->getRequest();
        // 这里的 SpanId 是指调用方分配给子请求的 SpanId
        $spanId = TraceDataHelper::getSubSpanIdFromHeaders($request->getHeaders());

        StatCollection::instance()->pushRequestInfo($spanId, $stats->getRequest());
        StatCollection::instance()->pushTransferInfo($spanId, $stats);

        if ($stats->hasResponse()) {
            StatCollection::instance()->pushResponseInfo($spanId, $stats->getResponse());
        }
    }
}
