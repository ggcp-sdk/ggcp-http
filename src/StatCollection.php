<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpHttp;

use DateTime;
use GgcpHttp\Support\Facades\Log;
use GgcpHttp\Support\Recorder\RecorderFactory;
use GgcpHttp\Support\Recorder\RecorderInterface;
use GgcpHttp\Support\TraceDataHelper;
use GgcpHttp\Support\Traits\SingletonTrait;
use GuzzleHttp\TransferStats;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use stdClass;

/**
 * Http 调用信息采集器。可用于采集请求的地址、入参、响应状态
 * 、响应数据、网络传输指标等数据
 *
 * @method static $this instance()
 */
class StatCollection
{
    use SingletonTrait;

    /**
     * 信息采集池
     *
     * @var array
     */
    private $pool = [];

    /**
     * 信息记录器
     *
     * @var RecorderInterface
     */
    private $recorder;

    /**
     * 采集器配置信息
     *
     * @var array
     */
    private $config;

    /**
     * 初始化采集器的一些配置信息
     *
     * @param  array $config
     * @return void
     */
    public function init(array $config)
    {
        if (!isset($config['debug'])) {
            // 默认开启 Debug，记录完整的业务信息
            $config['debug'] = true;
        }

        $engine  = $config['engine'] ?? 'mongo';
        $options = $config['options'];
        if ($engine === 'mongo') {
            // MongoDB 的一些默认配置（目前也仅支持 MongoDB）
            if (empty($options['host'])) {
                $options['host'] = 'mongodb://127.0.0.1:27017';
            }
            if (empty($options['dbname'])) {
                $options['dbname'] = 'httpTrace';
            }
            if (empty($options['collection'])) {
                $options['collection'] = 'dataSet';
            }
            if (empty($options['connectTimeout'])) {
                // 数据采集的场景，宁愿采集失败，都不能影响业务的正常性能。因此 MongoDB 的连接超时时间默认设置为 200ms
                $options['connectTimeout'] = 200;
            }
        }

        try {
            $this->recorder = RecorderFactory::make($engine, $options);
        } catch (\Exception $e) {
            Log::error('数据采集器初始化失败: ' . $e->getMessage());
        }

        $this->config = $config;
    }

    /**
     * @return bool
     */
    public function isPrepareRecorder()
    {
        return !is_null($this->recorder);
    }

    /**
     * @return bool
     */
    public function isDebug()
    {
        // 默认开启 Debug，否则目前业务不方便查看记录下来的请求调用日志信息
        return is_bool($this->config['debug']) ? $this->config['debug'] : true;
    }

    /**
     * 记录请求发起的开始时间
     */
    public function markRequestStartTime($spanId)
    {
        // 只有当 PHP 版本大于 7.1 时，DateTime 才支持输出包含毫秒的格式化内容
        // 因此小于 7.1 的 PHP 版本下，需要自行拼接毫秒数据
        if (version_compare(PHP_VERSION, '7.1', '>=')) {
            $time = (new DateTime)->format('Y-m-d H:i:s.u');
        } else {
            list($d, $u) = explode('.', microtime(true));
            $time        = date('Y-m-d H:i:s.', $d) . $u;
        }
        $this->pool[$spanId]['duration']['startTime'] = $time;
    }

    /**
     * 记录请求结束的结束时间
     */
    public function markRequestEndTime($spanId)
    {
        if (version_compare(PHP_VERSION, '7.1', '>=')) {
            $time = (new DateTime)->format('Y-m-d H:i:s.u');
        } else {
            list($d, $u) = explode('.', microtime(true));
            $time        = date('Y-m-d H:i:s.', $d) . $u;
        }
        $this->pool[$spanId]['duration']['endTime'] = $time;
    }

    /**
     * 往数据池推送一个请求的 Request 部分数据信息
     *
     * @param  string $spanId 调用方分配的请求唯一标识 ID
     * @param  RequestInterface $request
     * @return void
     */
    public function pushRequestInfo($spanId, RequestInterface $request)
    {
        $data = [
            'scheme'        => $request->getUri()->getScheme(),
            'host'          => $request->getUri()->getHost(),
            'query'         => $request->getUri()->getQuery(),
            'url'           => (string) $request->getUri(),
            'method'        => $request->getMethod(),
            'body'          => (string) $request->getBody(),
            'header'        => $this->formatGuzzleHeaders($request->getHeaders()),
            'protocVersion' => $request->getProtocolVersion(),
        ];

        // 从调用方的请求头获取调用链唯一标识 ID
        $this->pool[$spanId]['traceId'] = TraceDataHelper::getTraceIdFromHeaders(getallheaders());
        // 当前发起请求给请求分配的唯一 Sub Span Id 值
        $this->pool[$spanId]['spanId'] = $spanId;
        // 调用方的 spanId，对于被子请求来说，是它的 SpanId
        $this->pool[$spanId]['parentId'] = TraceDataHelper::getCurrentSpanId();
        // 调用方的 log 唯一标识 ID
        $this->pool[$spanId]['parentLogId'] = TraceDataHelper::getCurrentSpanId();
        // 当前调用方的 URL
        $this->pool[$spanId]['parentUrl'] = TraceDataHelper::getCurrentUrl();
        // 发起请求的 URL
        $this->pool[$spanId]['requestUrl']  = $data['url'];
        $this->pool[$spanId]['requestStat'] = $data;
    }

    /**
     * 往数据池推送一个请求的 Transfer 部分数据信息
     *
     * @param  string $spanId 调用方分配的请求唯一标识 ID
     * @param  TransferStats $transferStats
     * @return void
     */
    public function pushTransferInfo($spanId, TransferStats $transferStats)
    {
        $stats = $transferStats->getHandlerStats();

        $data = [
            'totalTime'      => $stats['total_time'],
            'namelookupTime' => $stats['namelookup_time'],
            'connectTime'    => $stats['connect_time'],
            'sslVerifyTime'  => $stats['pretransfer_time'],
            'waitTime'       => $stats['starttransfer_time'],
            'uploadSize'     => $stats['size_upload'],
            'uploadSpeed'    => $stats['speed_upload'],
            'downloadSize'   => $stats['size_download'],
            'downloadSpeed'  => $stats['speed_download'],
        ];

        $this->pool[$spanId]['transferStat'] = $data;
    }

    /**
     * 采集一个请求的响应 Response 部分数据信息
     *
     * @param  string $spanId 调用方分配的请求唯一标识 ID
     * @param  ResponseInterface $response
     * @return void
     */
    public function pushResponseInfo($spanId, ResponseInterface $response)
    {
        $responseHeaders = $this->formatGuzzleHeaders($response->getHeaders());

        $data = [
            'statusCode' => $response->getStatusCode(),
            'header'     => $responseHeaders,
            'content'    => (string) $response->getBody(),
        ];

        $responseSpanId = TraceDataHelper::getResponseSpanId($responseHeaders);
        // 如果能在请求响应的 Header 中找到 SpanId 数据，则优先使用请求响应的数据
        // 否则用当前请求发起时分配的 SpanId 作为子请求的 SpanId
        // 以子请求的 SpanId 为准，可以方便跟子请求所属服务的日志建立关联
        $this->pool[$spanId]['spanId']        = $responseSpanId ?: $spanId;
        $this->pool[$spanId]['responseLogId'] = $responseSpanId;
        $this->pool[$spanId]['responseStat']  = $data;
    }

    /**
     * 从 GuzzleHttp 的 Request 跟 Response 等类中的 getHeaders 方法中
     * 获取到的 Value 值是数组的格式，后续展示跟处理不太友好，因此优化成将 Values 数组拼接成一个字符串
     *
     * @param  array $headers 从 GuzzleHttp 中取到的请求头数组
     * @return array
     */
    protected function formatGuzzleHeaders(array $headers)
    {
        $result = [];
        foreach ($headers as $k => $vs) {
            $result[$k] = implode(', ', $vs);
        }
        return $result;
    }

    /**
     * 采集一个请求的异常信息
     *
     * @param  string $spanId    调用方分配的请求唯一标识 ID
     * @param  string $errMsg   错误描述信息
     * @return void
     */
    public function pushErrorInfo($spanId, $errMsg)
    {
        $this->pool[$spanId]['errMsg'] = $errMsg;
    }

    /**
     * 将采集到的数据发送到指定存储地，并清除本地数据缓存
     */
    public function sendAndClean($spanId = null)
    {
        if (!is_null($spanId)) {
            if (isset($this->pool[$spanId])) {
                $this->saveOne($this->pool[$spanId]);
                unset($this->pool[$spanId]);
            }
        } else {
            $this->saveAll($this->pool);
            unset($this->pool);
            $this->pool = [];
        }
    }

    /**
     * @param  array $data
     */
    protected function saveOne($data)
    {
        $tmpData = [
            'traceId'       => $data['traceId'],
            'spanId'        => $data['spanId'],
            'parentId'      => $data['parentId'],
            'parentLogId'   => $data['parentLogId'],
            'parentUrl'     => $data['parentUrl'],
            'requestUrl'    => $data['requestUrl'],
            'responseLogId' => $data['responseLogId'] ?? '',
            'duration'      => $data['duration'],
            'transferStat'  => $data['transferStat'],
            'requestStat'   => $data['requestStat'],
            'responseStat'  => $data['responseStat'] ?? [],
        ];

        if (isset($data['errMsg'])) {
            $tmpData['errMsg'] = $data['errMsg'];
        }

        $jsonStr = json_encode($tmpData, JSON_UNESCAPED_UNICODE);
        if ($this->isPrepareRecorder()) {
            $resId = $this->recorder->save($tmpData);
            if ($resId === false) {
                // 采集数据保存失败，在本地日志记录完整的数据信息
                Log::warning('GgcpHttp 数据采集器保存数据失败');
                Log::info('接口请求采集的数据: ' . $jsonStr);
            } else {
                // 采集数据保存成功的情况下，可根据服务自行需要选择是否记录完整的数据信息
                $this->isDebug() ? Log::info('接口请求采集的数据: ' . $jsonStr) : Log::info('接口请求采集保存的数据 ID: ' . $resId);
            }
        } else {
            // 如果数据采集器未准备，则在本地日志中记录数据信息
            Log::info('接口请求采集的数据: ' . $jsonStr);
        }

        // 如果接口请求失败，接口没有响应信息或 HTTP 状态码在 400 及以上，则单独记录一条 error 日志，方便日志系统做信息分析
        if (
            (isset($tmpData['responseStat']['statusCode']) && $tmpData['responseStat']['statusCode'] >= 400)
            || empty($tmpData['responseStat'])
        ) {
            Log::error("接口请求异常（详情可查看 info 日志） [{$tmpData['requestUrl']}] err: " . ($tmpData['errMsg'] ?: $tmpData['responseStat']['content']));
        }

        // 方便单元测试用
        if (isset($this->config['onStatCollection']) && is_callable($this->config['onStatCollection'])) {
            call_user_func($this->config['onStatCollection'], $tmpData);
        }
    }

    /**
     * @param  array $datas
     */
    protected function saveAll($datas)
    {
        foreach ($datas as $data) {
            $this->saveOne($data);
        }
    }
}
